public class FizzBuzz {

    private static final String FIZZ_BUZZ = "FizzBuzz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZ = "Fizz";
    private static final int FIZZ_BUZZ_DIVISOR = 15;
    private static final int BUZZ_DIVISOR = 5;
    private static final int FIZZ_DIVISOR = 3;

    public String returnWord(int i){
        if (i % FIZZ_BUZZ_DIVISOR == 0) {
            return FIZZ_BUZZ;
        } else if (i % BUZZ_DIVISOR == 0) {
            return BUZZ;
        } else if (i % FIZZ_DIVISOR == 0) {
            return FIZZ;
        } else {
            return Integer.toString(i);
        }
    }
}
