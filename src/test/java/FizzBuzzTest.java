import org.junit.*;
import static org.assertj.core.api.Assertions.*;

public class FizzBuzzTest {

    FizzBuzz fizzBuzz;

    @Before
    public void setUp() {
        fizzBuzz = new FizzBuzz();
    }

    @Test
    public void should_return_fizz_when_input_is_3 () {
        //given
        int numberGiven = 3;

        //when
        String fizz = fizzBuzz.returnWord(numberGiven);

        //then
        assertThat(fizz).isEqualTo("Fizz");
    }

    @Test
    public void should_return_buzz_when_input_is_20 () {
        //given
        int numberGiven = 20;

        //when
        String buzz = fizzBuzz.returnWord(numberGiven);

        //then
        assertThat(buzz).isEqualTo("Buzz");
    }

    @Test
    public void should_return_fizzbuzz_when_input_is_45() {
        //given
        int numberGiven = 45;

        //when
        String wordFizzBuzz = fizzBuzz.returnWord(numberGiven);

        //then
        assertThat(wordFizzBuzz).isEqualTo("FizzBuzz");
    }

    @Test
    public void should_return_19_when_input_is_19() {
        //given
        int numberGiven = 19;

        //when
        String nineteen = fizzBuzz.returnWord(numberGiven);

        //then
        assertThat(nineteen).isEqualTo("19");
    }
}
